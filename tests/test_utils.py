from barim.utils import spacer


def test_spacer() -> None:
    """
    Test that spacer function return the correct number of whitespace
    """
    lstring = "Hello"
    rstring = "World"
    max_length = 40

    assert (max_length - len(lstring) + (len(lstring) + len(rstring))) == len(spacer(lstring, rstring))
