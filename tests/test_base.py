from types import SimpleNamespace

import pytest

from barim import Command, Option

# noinspection PyProtectedMember
from barim.base import _GlobalOption


def test_command_global_options_post_init() -> None:
    """
    Test a global command option post init raise an error if action and store param are provided
    """
    with pytest.raises(ValueError):
        _GlobalOption(short="t", long="test", description="Test description", action="print_help", store=True)


def test_command_option_post_init() -> None:
    """
    Test a command option post init restriction
    """
    # Short must be 1 character
    with pytest.raises(ValueError):
        Option(short="wrong", long="wrongs", description="Test description")

    # Short must be alpha
    with pytest.raises(ValueError):
        Option(short="1", long="wrongs", description="Test description")

    # Short cannot contain special character
    with pytest.raises(ValueError):
        Option(short="-", long="wrongs", description="Test description")

    # Long cannot be len <= 1
    with pytest.raises(ValueError):
        Option(short="l", long="l", description="Test description")

    # Long must be alphabetic
    with pytest.raises(ValueError):
        Option(short="l", long="1234", description="Test description")

    # Long cannot contain special character
    with pytest.raises(ValueError):
        Option(short="l", long="lo-fi", description="Test description")

    # Too many actions set
    with pytest.raises(ValueError):
        Option(short="l", long="long", description="Test description", store=True, append=True)


def test_command_override_handle(command) -> None:
    """
    Test that command raise a NotImplementedError exception if handle as not been override
    """
    with pytest.raises(NotImplementedError):
        command.handle(SimpleNamespace(), SimpleNamespace())


def test_application_parser_global_options(application) -> None:
    """
    Test that application parser registered needed global options
    """
    _, opts = application.parser.parse(["test"])

    assert hasattr(opts, "help") and opts.help is False
    assert hasattr(opts, "version") and opts.version is False
    assert hasattr(opts, "verbose") and opts.verbose == 0


def test_application_parser(application) -> None:
    """
    Test application parser parsing results
    """
    parser = application.parser
    argv, opts = parser.parse(["test", "--verbose"])

    assert argv.command == "test"
    assert opts.verbose == 1


def test_application_parser_error(application) -> None:
    """
    Test application.parser.error is correctly exiting the program
    """
    with pytest.raises(SystemExit):
        application.parser.error("Test error message")


def test_application_match_command(application, command) -> None:
    """
    Test application.match_command results
    """
    assert application.match_command(command.name) == command


def test_application_run_error_default_multiple_command(application) -> None:
    """
    Test that running an application w/ multiple declared as default will raise an error
    """
    new_command = Command(name="new_command")
    application.register(new_command)

    with pytest.raises(RuntimeError):
        application.run(default=True)
