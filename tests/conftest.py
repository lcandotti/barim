import pytest

from barim import Application, Argument, Command, Option


@pytest.fixture
def command() -> Command:
    return Command(
        name="test_command",
        description="This is a test command",
        version="0.0.1",
        arguments=[Argument(name="input", description="Test argument")],
        options=[Option(short="t", long="test", description="Test option")],
    )


@pytest.fixture
def application(command) -> Application:
    application = Application(name="test_application", description="This is a test application", version="0.0.1")
    application.register(command)
    return application
