import os
from types import SimpleNamespace

from barim import Application, Command, Argument, Option

__doc__ = """
This file only purpose is to provide a working example of barim
"""


class MyCommand(Command):
    name = "my_command"
    description = "Print 'Hello, {input}!'"
    version = "0.1.0"

    arguments = [
        Argument(
            name="input",
            description="Use input string in print statement"
        ),
    ]
    options = [
        Option(
            short="u",
            long="upper",
            description="Turn all the letter uppercase",
        ),
    ]

    def handle(self, argv: SimpleNamespace, opts: SimpleNamespace) -> None:
        input_ = argv.input
        if opts.upper:
            input_ = input_.upper()

        print(f"Hello, {input_}")


def main() -> None:
    application = Application(name=__file__.split(os.sep)[-1])
    application.register(MyCommand)
    application.run(default=True)


if __name__ == "__main__":
    main()
