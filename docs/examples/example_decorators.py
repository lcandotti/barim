import os
from types import SimpleNamespace

from barim import Application, decorators

__doc__ = """
This file only purpose is to provide a working example of barim
"""


@decorators.command(name="hello")
def hello_world(argv: SimpleNamespace, opts: SimpleNamespace):
    print("Hello, World!")


if __name__ == "__main__":
    application = Application(name=__file__.split(os.sep)[-1])
    application.run()
