import os
from time import sleep
from types import SimpleNamespace
from typing import Dict, Optional

from barim import Application, Command, Option, ansi

__doc__ = """
This file only purpose is to provide a working example of barim
"""


class HelloCommand(Command):
    """"""

    name = "hello"
    description = "Print 'Hello, World!' with some styling"
    version = "0.1.0"

    def handle(self, argv: SimpleNamespace, opts: SimpleNamespace) -> None:
        print(f"Hello, {ansi.GREEN}World{ansi.RESET}!")


class PingPongCommand(Command):
    """"""

    name = "ping"
    description = "Print ping then load a progress bar and print pong after 1 second"
    version = "0.1.0"

    options = [Option(short="u", long="unicode", description="Change progress bar to unicode format")]

    def handle(self, argv: SimpleNamespace, opts: SimpleNamespace) -> None:
        print("\033[2J\033[H")

        border_start_char = "(" if opts.unicode else "["
        border_end_char = ")" if opts.unicode else "]"
        fill_char = "\u2588" if opts.unicode else "#"

        icon_frames = ["\u280B", "\u2819", "\u283C", "\u2834", "\u2826", "\u2827", "\u280F"]

        print(f"ping {border_start_char}{' ' * 50}{border_end_char} _", end="\r")

        placeholder_char = ansi.wraps([ansi.fg_from_id(59)], "\u2588") if opts.unicode else " "

        counter = 0
        progress_bar: Optional[str] = None

        for i in range(51):
            progress_bar = fill_char * i + placeholder_char * (50 - i)

            print(f"{icon_frames[counter]} {ansi.wrap(ansi.Color.RED, 'ping')} {border_start_char}{progress_bar}{border_end_char} _", end="\r")

            counter += 1
            if counter == 6:
                counter = 0

            sleep(0.15)

        print(f"{ansi.wrap(ansi.Color.GREEN, 'ping')} {border_start_char}{progress_bar}{border_end_char} {ansi.wrap(ansi.Color.GREEN, 'pong')}")


def main() -> None:
    application = Application(name=__file__.split(os.sep)[-1])
    application.register(HelloCommand).register(PingPongCommand)
    application.run()


if __name__ == "__main__":
    main()
