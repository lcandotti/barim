
Quickstart
^^^^^^^^^^

This following section is dedicated to explain how barim can be used to
create a simple CLI application. We will see how to make a terminal application
with a default command and how we can update it to allow multiple subcommand.

Create your first command
.........................

The first step will be to create a class that inherit from ``barim.Command`` and
provide some values to the class attributes.

.. code-block:: python
    :caption: example.py
    :linenos:

    from types import SimpleNamespace

    from barim import Command


    class MyCommand(Command):

        name = "example.py"
        description = "Print 'Hello, World!'"
        version = "0.1.0"

        def handle(self, argv: SimpleNamespace, opts: SimpleNamespace) -> None:
            print("Hello, World!")

Take note that the class attributes ``description`` and ``version`` aren't mandatory.
Only ``name`` is.

.. warning::

    The **handle()** method **must be override** has it will be the entry point for your
    software. The argv and opts parameters are two SimpleNamespace that contain
    all the arguments (argv) and all the options (opts) declared in the
    command class. To see how to use them check out the ``Barim API`` or just continue
    the quickstart guide.

The next logical step now is just to tell barim to run the command when the
script is run. This can be done like the following.

.. code-block:: python
    :caption: example.py
    :emphasize-added: 16-23
    :linenos:

    from types import SimpleNamespace

    from barim import Application, Command


    class MyCommand(Command):

        name = "my_command"
        description = "Print 'Hello, World!'"
        version = "0.1.0"

        def handle(self, argv: SimpleNamespace, opts: SimpleNamespace) -> None:
            print("Hello, World!")


    def main() -> None:
        application = Application(name="example.py")
        application.register(MyCommand)
        application.run(default=True)


    if __name__ == "__main__":
        main()

That should be all ! You can now test your CLI application by running the
following command.

.. code-block:: shell

    $ python example.py --help


The output of the command should be:

.. code-block:: shell

    $ python example.py --help
    example.py version 0.1.0

    DESCRIPTION
        Print 'Hello, World!'

    USAGE
        example.py  [-h] [-V] [-v]

    GLOBAL OPTIONS
        -h (--help)             Display help message
        -V (--version)          Display version number
        -v (--verbose)          Display more log message

.. note::

    Something to notice here is that the output doesn't show the correct colors.
    This will not be the case on the terminal.

Add arguments and options to the command
........................................

Let's say now that we want to take a string as an argument to print ``Hello, {input}!``.
For this we need to declare another class variable named ``arguments`` that
is a list of ``barim.Argument``.

.. code-block:: python
    :caption: example.py
    :emphasize-added: 11-16, 20
    :emphasize-removed: 19
    :linenos:

    from types import SimpleNamespace

    from barim import Application, Argument, Command


    class MyCommand(Command):
        name = "my_command"
        description = "Print 'Hello, {input}!'"
        version = "0.1.0"

        arguments = [
            Argument(
                name="input",
                description="Use input string in print statement"
            ),
        ]

        def handle(self, argv: SimpleNamespace, opts: SimpleNamespace) -> None:
            print("Hello, World!")
            print(f"Hello, {argv.input}")


    def main() -> None:
        application = Application(name="example.py")
        application.register(MyCommand)
        application.run(default=True)


    if __name__ == "__main__":
        main()

.. tip::

    Note that you can declare a barim.Argument or by providing the needed data
    during initialization like in the example above or by subclassing it and declaring the data as class variable.

You should now be able to run your script like the following.

.. code-block:: shell

    $ python example.py Demo


Expected output:

.. code-block:: shell

    $ python example.py Demo
    Hello, Demo!

Now when it comes to options, it doesn't change that much. Instead of declaring
arguments you declare options and provide a list of ``barim.Option``.
In our example let's say we want to turn all the letter uppercase.

.. code-block:: python
    :caption: example.py
    :emphasize-added: 17-23, 27-31
    :emphasize-removed: 26
    :linenos:

    from types import SimpleNamespace

    from barim import Application, Argument, Command, Option


    class MyCommand(Command):
        name = "my_command"
        description = "Print 'Hello, {input}!'"
        version = "0.1.0"

        arguments = [
            Argument(
                name="input",
                description="Use input string in print statement"
            ),
        ]
        options = [
            Option(
                short="u",
                long="upper",
                description="Turn all the letter uppercase",
            ),
        ]

        def handle(self, argv: SimpleNamespace, opts: SimpleNamespace) -> None:
            print(f"Hello, {argv.input}")
            input_ = argv.input
            if opts.upper:
                input_ = input_.upper()

            print(f"Hello, {input_}")


    def main() -> None:
        application = Application(name="example.py")
        application.register(MyCommand)
        application.run(default=True)


    if __name__ == "__main__":
        main()

Now run your script as below.

.. code-block:: shell

    $ python example.py demo --upper


Expected output:

.. code-block:: shell

    $ python example.py demo --upper
    Hello, DEMO!


Create subcommands
..................

The default parameter declare earlier in ``application.run(default=True)`` indicate that we only have one registered
command and that we want to use it as the default one. By removing this parameter we can now register commands
to act as subcommands.

.. code-block:: python
    :caption: example.py
    :emphasize-added: 33-39, 45
    :linenos:

    from types import SimpleNamespace

    from barim import Application, Argument, Command, Option


    class MyCommand(Command):
        name = "my_command"
        description = "Print 'Hello, {input}!'"
        version = "0.1.0"

        arguments = [
            Argument(
                name="input",
                description="Use input string in print statement"
            ),
        ]
        options = [
            Option(
                short="u",
                long="upper",
                description="Turn all the letter uppercase",
            ),
        ]

        def handle(self, argv: SimpleNamespace, opts: SimpleNamespace) -> None:
            input_ = argv.input
            if opts.upper:
                input_ = input_.upper()

            print(f"Hello, {input_}")


    class MyOtherCommand(Command):
        name = "my_other_command"
        description = "Print 'Hello, World!'"
        version = "0.1.0"

        def handle(self, argv: SimpleNamespace, opts: SimpleNamespace) -> None:
            print("Hello, World")


    def main() -> None:
        application = Application(name="example.py")
        application.register(MyCommand)
        application.register(MyOtherCommand)
        application.run()


    if __name__ == "__main__":
        main()

Create command dynamically
..........................

As seen before, to create a command, we have to subclass ``barim.Command``.
But this is not the only way we can create them. In any case you need to create them,
for example on the fly, you could do it like in the following example.

.. code-block:: python
    :caption: example.py
    :linenos:

    from types import SimpleNamespace

    from barim import Application, Command


    def my_command_handle(argv: SimpleNamespace, opts: SimpleNamespace) -> None:
        print("Hello, World")


    def main() -> None:
        my_command = Command(name="my_command", description="Print 'Hello, World!'", handle=my_command_handle)

        application = Application(name="example.py")
        application.register(my_command)
        application.run(default=True)


    if __name__ == "__main__":
        main()

The output of the command should look like the following:

.. code-block:: shell

    $ python example.py --help
    example.py version 0.1.0

    DESCRIPTION
        Print 'Hello, World!'

    USAGE
        example.py  [-h] [-V] [-v]

    GLOBAL OPTIONS
        -h (--help)             Display help message
        -V (--version)          Display version number
        -v (--verbose)          Display more log message

Create command with decorators
..............................

For people that loves more the ``click`` way, there's decorators available that will leverage all the work for you !
Here's a working example on how the decorators works.

.. code-block:: python
    :caption: example.py
    :linenos:

    from types import SimpleNamespace

    from barim import Application, decorators


    @decorators.command(name="hello", description="Some description")
    def hello_world(argv: SimpleNamespace, opts: SimpleNamespace):
        print("Hello, World!")


    if __name__ == "__main__":
        application = Application(name="example.py")
        application.run()

.. note::

    The decorator ``command`` accept the same parameters as ``barim.Command``

The expected output should be:

.. code-block:: shell

    $ python example.py --help
    example.py version 0.1.0

    DESCRIPTION
        Print 'Hello, World!'

    USAGE
        example.py  [-h] [-V] [-v]

    GLOBAL OPTIONS
        -h (--help)             Display help message
        -V (--version)          Display version number
        -v (--verbose)          Display more log message

    AVAILABLE COMMANDS
        hello                   Some description
