Installation
============

This section describe how to install barim in your project


Requirements
------------

- Python (`3.7.X or later <https://www.python.org/>`_)

Dev requirements
----------------

- Git (`latest version <https://git-scm.com/>`_)
- Python (`3.7.X or later <https://www.python.org/>`_)
- Poetry (`latest version <https://python-poetry.org>`_)

Installing
----------

Install with pip.

.. code-block:: bash

    python -m pip install barim


Install with poetry.

.. code-block:: bash

    poetry add barim
