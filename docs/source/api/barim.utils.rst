﻿barim.utils
===========

.. automodule:: barim.utils
    :members:
    :undoc-members:
    :private-members:
    :show-inheritance:

    Summary
    =======

    .. rubric:: Functions

    .. autosummary::

        spacer

    Members
    =======
