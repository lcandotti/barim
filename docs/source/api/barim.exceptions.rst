﻿barim.exceptions
================

.. automodule:: barim.exceptions
    :members:
    :undoc-members:
    :private-members:
    :show-inheritance:

    Summary
    =======

    .. rubric:: Exceptions

    .. autosummary::

        CommandDuplicateError

    Members
    =======
