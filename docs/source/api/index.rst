API Documentation
=================

This section give all the insight on barim internal API. The document is presented in a way
to represent the structure of barim module.

.. toctree::

    barim
    barim.abstracts
    barim.ansi
    barim.base
    barim.decorators
    barim.exceptions
    barim.utils
