﻿barim.ansi
==========

.. automodule:: barim.ansi
    :members:
    :undoc-members:
    :private-members:
    :show-inheritance:

    Summary
    =======

    .. rubric:: Functions

    .. autosummary::

        bg_from_id
        bg_from_rgb
        error
        fg_from_id
        fg_from_rgb
        success
        warning
        wrap
        wraps


    .. rubric:: Classes

    .. autosummary::

        Color
        Style

    Members
    =======
