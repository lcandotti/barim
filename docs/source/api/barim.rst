barim
=====

All the classes declared in the section below can be found in ``barim.base``

.. automodule:: barim
    :members:
    :show-inheritance:


    Summary
    =======

    .. rubric:: Classes

    .. autosummary::

        Application
        Argument
        Command
        Option

    Members
    =======
