barim.abstracts
===============

.. automodule:: barim.abstracts
    :members:
    :undoc-members:
    :private-members:
    :show-inheritance:

    Summary
    =======

    .. rubric:: Classes

    .. autosummary::

        _DecoratedSingleton

    Members
    =======

