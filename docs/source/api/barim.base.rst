﻿barim.base
==========

.. vale Awesome.SpellCheck = YES

.. automodule:: barim.base
    :members:
    :undoc-members:
    :private-members:
    :show-inheritance:

    Summary
    =======

    .. rubric:: Classes

    .. autosummary::

        Application
        Argument
        Command
        Option

    Members
    =======
