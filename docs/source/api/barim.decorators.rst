barim.decorators
================

.. automodule:: barim.decorators
    :members:
    :undoc-members:
    :private-members:
    :show-inheritance:

    Summary
    =======

    .. rubric:: Functions

    .. autosummary::

        command

    Members
    =======
