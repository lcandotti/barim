.. Barim documentation master file, created by
   sphinx-quickstart on Mon Jan 17 02:40:31 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Barim's documentation!
=================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

Barim is a Python library that helps building CLI applications.

The Barim API makes it easy to build terminal app that have one or more commands.

Contents table
^^^^^^^^^^^^^^

.. toctree::

   about
   license
   usage/index
   api/index
