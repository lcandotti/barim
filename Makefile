bandit:
	@poetry run bandit -r barim

coverage:
	@poetry run coverage run -m pytest
	@poetry run coverage html --omit="*test*"

mypy:
	@poetry run mypy barim --config-file pyproject.toml $(opt)

pytest:
	@poetry run pytest

tox:
	@poetry run tox $(opt)

ci: bandit mypy tox
